FROM nginx:latest

COPY . /usr/share/nginx/html

EXPOSE 8007


CMD ["nginx", "-g", "daemon off;"] 
